/**
 * On peut ajouter des eventListener sur n'importe quel élément
 * html via le javascript. L'eventListener va surveiller un
 * événement précis sur l'élément en question, et déclencher
 * une fonction au moment où cet événement sera lancé.
 */
let btn = document.querySelector('button');
let para = document.querySelector('#para');
/*
On utilise la fonction addEventListener qui attend en premier argument
l'event à surveiller en string et en deuxième argument la fonction
à déclencher lorsque cet event est lancé.
Ici, on surveille l'event click sur un button, et on utilise une
fonction anonyme (une fonction sans nom qui n'a pas vocation à 
être réutilisée ailleurs ou exécuter par nous manuellement) qui
fera un console log et changera la couleur du paragraphe en rouge
*/
btn.addEventListener('click', function() {
    console.log('bloup');
    para.style.color = 'red';
});
/*
Ici, on surveille les double-clicks sur le paragraphe pour changer
sa background-color en vert quand ça arrivera
*/
para.addEventListener('dblclick', function() {
    para.style.backgroundColor = 'green';
});
