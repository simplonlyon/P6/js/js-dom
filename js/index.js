"use strict";
/**
 * Le querySelector va permettre de sélectionner un élément HTML
 * en se basant sur un sélecteur CSS.
 * Le javascript capturera le premier élément qu'il trouve dans le
 * HTML qui correspond au sélecteur.
 * Ici, on sélectionne un élément qui a un id égal à para.
 */
let p = document.querySelector('#para');
/**
 * Une fois un élément sélectionner, on peut modifier tout ce qu'on
 * veut sur cet élément, le contenu textuel, les classes, les id,
 * le style etc. Tous les attributs html sont disponibles en lecture
 * et en ecriture via le javascript.
 * Ici, on remplace le texte de l'élément par "with something else"
 */
p.textContent = 'with something else';
//Ici, on rajoute une class classe1 à l'élément
p.classList.add('classe1');
//Ici, on dit que si l'élément possède une classe maClasse
if(p.classList.contains('maClasse')) {
    //On passe sa couleur en vert
    p.style.color = 'green';
}

/*
On peut récupéré le body avec un querySelector ou directement
sur le document
*/
// let body = document.querySelector('body');
let body = document.body;
//On lui assigne une background-color en rgb en lui donnant le
//résultat de la fonction getRandomColor pour le red le green et le blue
body.style.backgroundColor = `rgb(${getRandomColor()}, ${getRandomColor()}, ${getRandomColor()})`;
/**
 * Cette fonction retourne une valeur ronde entre 1 et 255
 */
function getRandomColor() {
    return Math.floor(Math.random() * (255 - 1 + 1)) + 1;
}



/*exemples de concaténations*/
// let variable = 'bloup';
//avec '' et "" on doit concaténer les variables avec des +
// 'Mon mot préféré est '+variable;
// 'mon nom est '+nom+' '+prenom+' je vais sur mes '+age+' ans';
//avec les ``, on peut directement mettre les variable dans la
//chaîne de caractères entre ${}, ce qui fait une concaténation plus propre
// `Mon mot préféré ${variable}`;
// `mon nom est ${nom} ${prenom} je vais sur mes ${age} ans`;