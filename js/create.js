/*
Pour créer des élément html directement via le javascript,
on utilise la fonction createElement du document, cette fonction
attend en argument le type de balise html que l'on veut créer.
Une fois qu'on l'a créé, l'élément devient disponible dans la variable
auquel on l'a assigné, comme si on l'avait sélectionné avec
un querySelector.
A ce moment là, il n'existe toujours pas dans le DOM, il n'existe
qu'en mémoire, mais ne sera pas visible ni rien dans le document.
On peut ceci dit le manipuler comme un élément classique.
*/
let p = document.createElement('p');
p.textContent = 'bloup';
p.id = 'monId';

/**
 * 1) Créer un élément section dans lequel vous mettrez le p, puis
 * ajouter la section au body plutôt que directement le p
 * 2) Créer un élément span que vous mettrez dans le p.
 * 3) Mettre un textContent au span
 * 4) Mettre le span en gras
 */

 //Pour ajouter l'élément, il faudra l'appendChild sur un élément
 //déjà présent dans le DOM de notre page. Ici, le body
document.body.appendChild(p); //à modifier par le 1)