//definition des elements html à capturer
let userInput = document.querySelector('.userInput');
let button = document.querySelector('button');
let pEmpty = document.querySelector('.emptyP');

//L'event input se déclencher à chaque modification de la valeur de l'input
userInput.addEventListener('input', function () {
    pEmpty.textContent = userInput.value;
});

button.addEventListener('click', function () {
    //pour récupérer la valeur d'un élément input/textarea/select
    //il faut utiliser la propriété .value de cet élément
    pEmpty.textContent = userInput.value;

    console.log('Ok');

    // lors d'un click sur le bouton, 
    // on execute une fonction qui
    //affecte la valeur (.value)de l'input
});