TP Events : (exercice-event.js)
1) Créer un fichier exercice-event.js et html
2) Dans le HTML, mettre un input, un button et un paragraphe, puis dans le js, faire que quand on click sur le button, on récupère la value de l'input pour la mettre en textContent du paragraphe
3) Faire que la value de l'input aille en textContent du paragraphe en temps réel quand on est en train de taper dans l'input (voir quel event des input permettra de faire ça)